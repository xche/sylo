export const extensionToLanguageObj = {
    '.js': "javascript",
    '.php': "php",
    '.xml': "xml",
    '.c': 'clike',
    '.css': 'css',
    '.md': 'markdown',
    ".py": 'python',
    ".sql": 'sql'
}
