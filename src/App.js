import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import {Provider} from 'react-redux';
import store from "./store";
import {Main} from "./page/Main";


function App() {
    return (
        <BrowserRouter>
            <Provider store={store}>
                <Route path={'/'} exact component={Main}/>
            </Provider>
        </BrowserRouter>
    );
}

export default App;
