import React, {Component, Fragment} from 'react';
import {EditGistHeader} from "../../components/EditGistHeader";
import {EditGistContent} from "../../components/EditGistContent/EditGistContent";
import {SubmitLine} from "../../components/SubmitLine";
import {connect} from 'react-redux';
import * as actions from './store/actions';

export class _Main extends Component {
    componentDidMount() {
        if (this.props.contentList.length < 1) {
            this.props.addAnotherTextArea(0);
        }
    }

    render() {
        const {contentList, lastId, changeTextAreaValueAccordingToId, addAnotherTextArea, deleteTextAreaAccordingToId, changeTextAreaExtensionAndLanguage} = this.props;
        return (
            <Fragment>
                <EditGistHeader/>
                {Array.isArray(contentList) && contentList.map((item, index) => (
                    <EditGistContent
                        contentValueObj={item}
                        changeTextAreaValueAccordingToId={changeTextAreaValueAccordingToId}
                        index={index} key={index + item}
                        contentLength={contentList.length}
                        deleteTextAreaAccordingToId={deleteTextAreaAccordingToId}
                        changeTextAreaExtensionAndLanguage={changeTextAreaExtensionAndLanguage}
                    />
                ))}
                <SubmitLine lastId={lastId} addAnotherTextArea={addAnotherTextArea} contentLength={contentList.length}/>
            </Fragment>
        )
    }
}

const mapState = state => ({
    contentList: state.main.contentList,
    lastId: state.main.lastId
});
const mapDispatch = dispatch => ({
    changeTextAreaValueAccordingToId: contentValueObj =>
        dispatch(actions.changeTextAreaValueAccordingToIdAction(contentValueObj)),
    addAnotherTextArea: lastId => dispatch(actions.addAnotherTextAreaAction(lastId)),
    deleteTextAreaAccordingToId: id => dispatch(actions.deleteTextAreaAccordingToIdAction(id)),
    changeTextAreaExtensionAndLanguage: contentObj => dispatch(actions.changeTextAreaExtensionAndLanguageAction(contentObj))
});
export const Main = connect(mapState, mapDispatch)(_Main);
