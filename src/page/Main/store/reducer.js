import * as actionTypes from './actionTypes';
import {extensionToLanguageObj} from "../../../helpers/extensions";

export const reducer = (state = {
    contentList: [{value: "", id: 0, extension: "", language: ""}],
    lastId: 2
}, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_TEXTAREA_VALUE_ACCORDING_TO_ID:
            const tmpStateForAdd = JSON.parse(JSON.stringify(state));
            tmpStateForAdd.contentList.forEach(item => {
                if (item.id === action.payload.id) {
                    item.value = action.payload.value
                }
            });
            return tmpStateForAdd;
        case actionTypes.DELETE_TEXTAREA_ACCORDING_TO_ID:
            let tmpStateForDel = JSON.parse(JSON.stringify(state));
            tmpStateForDel.contentList = tmpStateForDel.contentList.filter(item => {
                if (item.id !== action.payload) {
                    return true;
                }
            });
            return tmpStateForDel;

        case actionTypes.ADD_ANOTHER_TEXTAREA:
            return {
                ...state,
                contentList: [...state.contentList, {id: action.payload + 1, value: '', extension: ''}],
                lastId: action.payload + 1
            };

        case actionTypes.CHANGE_TEXTAREA_EXTENSION_N_LANGUAGE:
            const tmpStateForExt = JSON.parse(JSON.stringify(state));
            tmpStateForExt.contentList.forEach(item => {
                if (item.id === action.payload.id) {
                    item.extension = action.payload.extension;
                    item.language = extensionToLanguageObj[action.payload.extension]
                }
            });
            return tmpStateForExt;
        default:
            return state;
    }
}
