import {createAction} from "redux-actions";
import * as types from "./actionTypes";

export const changeTextAreaValueAccordingToIdAction = contentObj =>
    createAction(types.CHANGE_TEXTAREA_VALUE_ACCORDING_TO_ID)(contentObj);
export const addAnotherTextAreaAction = lastId =>
    createAction(types.ADD_ANOTHER_TEXTAREA)(lastId);
export const deleteTextAreaAccordingToIdAction = lastId =>
    createAction(types.DELETE_TEXTAREA_ACCORDING_TO_ID)(lastId);
export const changeTextAreaExtensionAndLanguageAction = contentObj =>
    createAction(types.CHANGE_TEXTAREA_EXTENSION_N_LANGUAGE)(contentObj);
export const fetchNotes = contentObj =>
    createAction(types.FETCH_NOTES)(contentObj);
