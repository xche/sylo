import {combineReducers} from 'redux';
import {reducer as mainReducer} from "../page/Main/store/reducer";

const reducer = combineReducers({
    main: mainReducer
});

export default reducer;
