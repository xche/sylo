import { takeEvery, put } from 'redux-saga/effects';
import {CHANGE_TEXTAREA_VALUE_ACCORDING_TO_ID} from '../page/Main/store/actionTypes';

const ipfsAPI = require("ipfs-http-client");
const ipfs = ipfsAPI({ host: "localhost", port: "5001", protocol: "http" });

function* fetchNotes() {
    try {
        // yield ipfs.files.mkdir('/hello-world', (err) => {
        //     if (err) {
        //       console.error(err)
        //     }
        // })
        // yield ipfs.files.write('/hello-world/test.txt', Buffer.from('Hello, world!'), (err) => {
        //     console.log(err)
        // })
        yield ipfs.files.stat('/hello-world/test.txt', (err, stats) => {
            console.log(stats)
        })
        
        // yield ipfs.files.read('/hello-world', (error, buf) => {
        //     console.log(buf.toString('utf8'));
        // });
        // yield put();
    }catch(e) {
        console.log(e);
    }
}

function* noteSaga() {
    yield takeEvery(CHANGE_TEXTAREA_VALUE_ACCORDING_TO_ID, fetchNotes);
}
  
export default noteSaga;