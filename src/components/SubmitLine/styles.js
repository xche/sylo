import styled from "styled-components";

export const SubmitLineContainer = styled.div`
width:60%;
height:60px;
line-height: 60px;
margin:-15px auto;
button{
height:40px;
padding: 0 10px;
font-size: 14px;
color:#555;
border-radius: 5px;
font-weight: bold;
background-color: #fafafa;
&.createSecretGist{
background-color: #e6e1c0;
}
}
.floatRightContainer{
float:right;
button{
margin-left: 10px;
}
}
`
