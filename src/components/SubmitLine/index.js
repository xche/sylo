import React, {PureComponent, Fragment} from 'react';
import {SubmitLineContainer} from './styles';

export class SubmitLine extends PureComponent {
    render() {
        const {addAnotherTextArea, lastId, contentLength} = this.props;
        return (
            <Fragment>
                <SubmitLineContainer>
                    <button className={"addFile"} onClick={async () => {
                        await addAnotherTextArea(lastId);
                        //adjust window and scrollY to 420*length
                        await window.scrollTo(0, 420 * contentLength);
                    }}>Add file
                    </button>
                    <div className={"floatRightContainer"}>
                        <button className={"createSecretGist"}>Create secrete gist</button>
                        <button className={"createPublicGist"}>Create public gist</button>
                    </div>
                </SubmitLineContainer>
            </Fragment>
        );
    }
}
