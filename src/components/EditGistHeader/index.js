import React, {PureComponent, Fragment} from 'react';
import {connect} from "react-redux";
import {EditDescription} from "./styles";

export class EditGistHeader extends PureComponent {
    render() {
        return (
            <Fragment>
                <EditDescription
                    placeholder={"Gist description"}/>
            </Fragment>
        )
    }
}
