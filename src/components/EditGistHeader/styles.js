import styled from 'styled-components';

export const EditDescription = styled.input`
display: block;
outline: none;
border: 1px #ddd solid;
font-size: 15px;
color:#999;
width:60%;
height:30px;
line-height: 30px;
margin:0 auto;
text-indent: 20px;
border-radius: 3px;
`;
