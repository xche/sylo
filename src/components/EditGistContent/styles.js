import styled from 'styled-components';

export const EditDescription = styled.input`
display: block;
outline: none;
border: 1px #ddd solid;
font-size: 15px;
color:#999;
width:60%;
height:30px;
line-height: 30px;
margin:0 auto;
text-indent: 20px;
border-radius: 3px;
`;

export const EditGistContainer = styled.div`
width: 55%;
height:400px;
outline: none;
border: 1px #ddd solid;
font-size: 15px;
color:#999;
line-height: 30px;
margin:20px auto;
text-indent: 20px;
border-radius: 6px;
//overflow: scroll;
.codeEditor{
//width: 100%;
height:400px !important;
//overflow: scroll;
//font-size: 15px;
//color: #444;
//outline: none;
//border: none;
//resize:none;
//line-height: 30px;
//box-sizing: border-box;
//padding: 0px 10px;
//background-color: pink;
}
`;

export const EditGistTextAreaHeader = styled.div`
width: 100%;
background-color: #f8f8f8;
height:50px;
line-height: 50px;
border-bottom: 1px solid #ddd;
button.deleteBtn{
outline: none;
height:39px;
width: 38px;
line-height: 38px;
border:1px #ddd solid;
//border-left: none;
margin-left: -1px;
border-bottom-right-radius: 4px;
border-top-right-radius: 4px;
&:hover{
background-color: #cb2431;
 svg.trashIcon{
fill:white;
}
}
}

svg.trashIcon{
fill:rgb(203, 36, 49)
//stroke-opacity:0.8;
}
input{
outline: none;
border: 1px #ddd solid;
margin-top: 6px;
margin-left: -10px;
font-size: 15px;
color:#666;
width:25%;
height:35px;
line-height: 35px;
text-indent: 10px;
border-radius: 3px;
}
select{
        height: 35px;
        color:#666;
        width: 100px;
        text-indent: 10px;
        line-height: 28px;
        outline: none;
        font-size: 15px;
        font-weight: normal;
        box-sizing: border-box;
        margin-right: 10px;
        appearance:none;
        -moz-appearance:none;
        -webkit-appearance:none;
        background: url(./arrow.png) no-repeat scroll 80px center transparent;
        background-size:8%;
        padding-right: 18px;
}
div{
display: inline-block;
float:right;
margin-right: 30px;
}
`

export const EditGistTextArea = styled.textarea`
width: 99.5%;
height:344px;
overflow: scroll;
font-size: 15px;
color: #444;
outline: none;
border: none;
resize:none;
line-height: 30px;
box-sizing: border-box;
padding: 0px 10px;

`
