import React, {Component, Fragment} from 'react';
import {EditGistContainer, EditGistTextAreaHeader, EditGistTextArea} from "./styles";
import {Controlled as CodeMirror} from 'react-codemirror2'

require('./codemirror.css');
// require('codemirror/theme/material.css');
require('codemirror/theme/neat.css');
require('codemirror/mode/xml/xml.js');
require('codemirror/mode/javascript/javascript.js');
require('codemirror/mode/sql/sql.js');
require('codemirror/mode/python/python.js');
require('codemirror/mode/php/php.js');
require('codemirror/mode/css/css.js');
require('codemirror/mode/markdown/markdown.js');
require('codemirror/mode/clike/clike.js');


export class EditGistContent extends Component {
    constructor(props){
        super(props);
        this.state={};
    }

    render() {
        const {contentValueObj, contentLength, changeTextAreaValueAccordingToId, deleteTextAreaAccordingToId, changeTextAreaExtensionAndLanguage} = this.props;
        return (
            <Fragment>
                <EditGistContainer>
                    <EditGistTextAreaHeader>
                        <input type="text"
                               placeholder={"File extension"}
                               onChange={e => {
                                   this.setState({extension: e.target.value})
                               }}
                               onBlur={e => {
                                   changeTextAreaExtensionAndLanguage({
                                       ...contentValueObj,
                                       extension: this.state && this.state.extension
                                   })
                               }}
                        />
                        {contentLength > 1 && <button type="button"
                                                      className="deleteBtn"
                                                      aria-label="Remove file"
                                                      onClick={() => deleteTextAreaAccordingToId(contentValueObj.id)}
                        >
                            <svg strokeWidth={0.5} className="trashIcon"
                                 viewBox="0 0 12 16" version="1.1" width="12"
                                 height="16" aria-hidden="true">
                                <path fillRule="evenodd"
                                      d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"></path>
                            </svg>
                        </button>}
                        <div>
                            <select onChange={e => {
                                this.setState({indentWithTabs: e.target.value === "tab"})
                            }}
                            >
                                <optgroup label="Indent mode">
                                    <option value="space">Spaces</option>
                                    <option value="tab">Tabs</option>
                                </optgroup>
                            </select>
                            <select onChange={e => {
                                this.setState({indentUnit: e.target.value})
                            }}>
                                <optgroup label="Indent size">
                                    <option value="2">2</option>
                                    <option value="4">4</option>
                                    <option value="8">8</option>
                                </optgroup>
                            </select>
                            <select onChange={e => {
                                this.setState({lineWrapping: e.target.value === "on"})
                            }}>
                                <optgroup label="Line wrap mode">
                                    <option value="off">No wrap</option>
                                    <option value="on">Soft wrap</option>
                                </optgroup>
                            </select>
                        </div>
                    </EditGistTextAreaHeader>
                    <CodeMirror className={"codeEditor"}
                                options={{
                                    mode: contentValueObj.language,
                                    lineNumbers: true,
                                    indentUnit: (this.state && parseInt(this.state.indentUnit)) || 2,
                                    lineWrapping: (this.state && this.state.lineWrapping) || false,
                                    indentWithTabs: (this.state && this.state.indentWithTabs) || false
                                }}
                                value={contentValueObj.value}
                                onBeforeChange={(editor, data, value) => {
                                    changeTextAreaValueAccordingToId({
                                        id: contentValueObj.id,
                                        value
                                    })
                                }}
                    />
                </EditGistContainer>
            </Fragment>
        )
    }
}
